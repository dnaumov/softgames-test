//
//  Notification.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation

struct Notification {
    let title: String
    let message: String
    let delay: TimeInterval
}
