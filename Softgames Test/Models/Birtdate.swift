//
//  Birtdate.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation

struct Birthdate: Codable {

    private let dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()

    let dateString: String

    private var date: Date? {
        return dateFormatter.date(from: dateString)
    }

    public var age: Int? {
        guard let date = date else {
            return nil
        }

        let ageInYears = Calendar.current.dateComponents([.year], from: date, to: Date()).year
        return ageInYears
    }

    enum CodingKeys: String, CodingKey {
        case dateString = "date"
    }
}
