//
//  User.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation

struct User: Codable {
    let firstName: String
    let lastName: String

    var fullName: String {
        let resutl = "\(firstName) \(lastName)"
        return resutl
    }

    enum CodingKeys: String, CodingKey {
        case firstName = "firstName"
        case lastName = "lastName"
    }
}
