//
//  String+JsonData.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation

extension String {
    func jsonStringToDictionary() -> [String: Codable]? {
        guard let data = self.data(using: .utf8), let dictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: String] else {
            return nil
        }
        return dictionary
    }
}

