//
//  NotificationManager.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation
import UserNotifications
import os.log

class NotificationManager {
    static let shared = NotificationManager()
    private init(){}

    /// Triggers the local notification
    /// - Parameter notification: notification to trigger
    func triggerNotification(_ notification: Notification) {
        // Check the notification settings
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .notDetermined {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge]) { (granted, error) in
                    self.triggerNotification(notification)
                }
            } else if settings.authorizationStatus == .denied {
                // TODO: Ask user to allow notifications
                os_log("Notifications denied")
            } else if settings.authorizationStatus == .authorized {

                let content = UNMutableNotificationContent()
                content.title = notification.title
                content.body = notification.message
                content.sound = .default

                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: (notification.delay), repeats: false)
                let notificationRequest = UNNotificationRequest(identifier: "LocalNotification", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(notificationRequest)
            }
        }
    }
}
