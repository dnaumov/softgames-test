
var userAgeDivId

function setFullUsername(firstName, lastName, resultDivId) {
    var type = "string"
    var resultDiv = document.getElementById(resultDivId)
    if (isParameterCorrect(firstName, type) && isParameterCorrect(lastName, type)) {
        try {
            var promptString = "{\"function\":\"fullName\", \"firstName\":\"" + firstName  + "\", \"lastName\":\"" + lastName + "\"}";
            var fullName = prompt(promptString);
            resultDiv.innerHTML = fullName;
        } catch(err) {
            console.log('The native context does not exist yet');
            resultDiv.innerHTML = "";
        }
    }
}

function triggerNotification() {
    if (window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.getAge) {
        window.webkit.messageHandlers.getAge.postMessage({ "function": "triggerNotification" });
    }
}

function getUserAge(birthDate, ageDivId) {
    userAgeDivId = ageDivId
    if (window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.getAge) {
        window.webkit.messageHandlers.getAge.postMessage({ "function": "getAge", "callbackFunction": "getUserAgeCallback", "date": birthDate });
    }
}

function getUserAgeCallback(ageString) {
    if (userAgeDivId != null) {
        document.getElementById(userAgeDivId).innerHTML = ageString;
    }
}

function isParameterCorrect(parameter, type) {
    if (parameter != null && typeof parameter === type) {
        return true;
    }
    return false;
}

