//
//  JsHandler.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation
import UIKit
import WebKit
import os.log

class JsHandler: Equatable {
    private(set) weak var webView: WKWebView?

    // Used to compare handlers
    private let id = UUID().uuidString

    let method: JsApi.Method
    let body: [String: Codable]

    required init(webView: WKWebView, method: JsApi.Method, body: [String: Codable]) {
        self.webView = webView
        self.method = method
        self.body = body
    }

    @discardableResult
    func proceed(completion: ((Any?)->Void)? = nil ) -> Any? {
        completion?(nil)
        return nil
    }

    func triggerJsCallback(fullJsMethod: String, completion: (()->Void)? = nil) {
        guard body["callbackFunction"] != nil else {
            completion?()
            return
        }

        webView?.evaluateJavaScript(fullJsMethod, completionHandler: { _, error in
            if let error = error {
                os_log("Error during JS evaluation: \(fullJsMethod). Error: \(error.localizedDescription)")
            }
            completion?()
        })
    }

    static func == (lhs: JsHandler, rhs: JsHandler) -> Bool {
        return lhs.id == rhs.id
    }

}

