//
//  NotificationHandler.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation
class NotificationHandler: JsHandler {
    override func proceed(completion: ((Any?) -> Void)? = nil) -> Any? {
        // It is an `fire and forget` async handler without any parameters

        // TODO: It is also would be possible to pass the notification data via the JS
        let notification = Notification(title: "Solitaire smash", message: "Play again to smash your top score", delay: 7)
        NotificationManager.shared.triggerNotification(notification)
        
        return nil
    }
}
