//
//  FullNameHandler.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation

// This is sync handler. Result is returned
class FullNameHandler: JsHandler {
    override func proceed(completion: ((Any?) -> Void)? = nil) -> Any? {
        guard let data = try? JSONSerialization.data(withJSONObject: body, options: []),
              let user = try? JSONDecoder().decode(User.self, from: data) else {
            return nil
        }
        return user.fullName as Any
    }
}

