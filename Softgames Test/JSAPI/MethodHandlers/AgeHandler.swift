//
//  AgeHandler.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation

// This is async handler. Result is passed into the completiond
class AgeHandler: JsHandler {
    override func proceed(completion: ((Any?) -> Void)? = nil) -> Any? {
        // 5 seconds delay as requested in the task
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: { [weak self] in
            guard let bodyData = self?.body,
                let data = try? JSONSerialization.data(withJSONObject: bodyData, options: []),
                let birtdate = try? JSONDecoder().decode(Birthdate.self, from: data),
                let ageInYears = birtdate.age else {
                      completion?(nil)
                      return
                  }

            // Get the callback function
            guard let callbackFunction = self?.body["callbackFunction"] else {
                completion?(nil)
                return
            }
            // Age callback function is "getUserAgeCallback(age)"
            let jsMethod = "\(callbackFunction)(\(ageInYears))"

            // Trigger JS. In future would be nice to add error communication layer
            self?.triggerJsCallback(fullJsMethod: jsMethod) {
                completion?(ageInYears)
            }
        })
        return nil
    }
}
