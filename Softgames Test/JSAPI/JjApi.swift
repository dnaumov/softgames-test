//
//  JSFunction.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import Foundation
import UIKit
import WebKit

enum JsApi {
    enum MethodType {
        case async
        case sync
    }

    enum Method: String, CaseIterable {
        /// `fullName` function
        case fullName
        /// `getAge` function
        case age = "getAge"
        /// `triggerNotification` function
        case notification = "triggerNotification"

        private var handlerType: JsHandler.Type {
            switch self {
                case .fullName:
                    return FullNameHandler.self
                case .age:
                    return AgeHandler.self
                case .notification:
                    return NotificationHandler.self
            }
        }

        /// Type of the method (sync or async)
        var methodType: MethodType {
            switch self {
                case .fullName:
                    return .sync
                case .age, .notification:
                    return .async
            }
        }

        /// Provides the function type
        /// - Parameter jsonString: JSON string from the web view
        /// - Returns: type of the function
        static func methodFrom(dictionary: [String: Codable]) -> Method? {
            guard let functionName = dictionary["function"] as? String, let result = Method(rawValue: functionName) else {
                return nil
            }

            return result
        }

        /// Creates the handler for the given method
        /// - Parameters:
        ///   - webView: web view
        ///   - body: dictionary with the function and parameters data
        /// - Returns: JS API handler
        func createHandler(webView: WKWebView, body: [String: Codable]) -> JsHandler {
            return handlerType.init(webView: webView, method: self, body: body)
        }
    }
}
