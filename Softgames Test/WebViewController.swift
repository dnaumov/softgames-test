//
//  ViewController.swift
//  Softgames Test
//
//  Created by Dmytro Naumov on 12.02.22.
//

import UIKit
import WebKit
import os.log

class WebViewController: UIViewController {

    @IBOutlet private weak var webView: WKWebView?

    private var activeHandlers = [JsHandler]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        injectJsMethods()
        loadHTML()
    }

    private func setupDelegates() {
        // Set the WebView delegates
        webView?.uiDelegate = self
    }

    func loadHTML() {
        let pageName = "user-data"
        guard let pageUrl = Bundle.main.url(forResource: pageName, withExtension: "html") else {
            os_log("Can't load local page: \(pageName)")
            return
        }

        webView?.load(URLRequest.init(url: pageUrl))
    }

    func injectJsMethods() {
        JsApi.Method.allCases.forEach { method in
            if method.methodType == .async {
                webView?.configuration.userContentController.add(self, name: method.rawValue)
            }
        }
    }
}

extension WebViewController: WKUIDelegate {

    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        guard prompt.contains("function") else {
        // TODO: in case prompts should be supported on this page - implement native AlertView
            completionHandler(nil)
            return
        }

        guard let dataFromString = prompt.jsonStringToDictionary(), let method = JsApi.Method.methodFrom(dictionary: dataFromString) else {
            os_log("JS data is not valid: \(prompt)")
            completionHandler(nil)
            return
        }

        let handler = method.createHandler(webView: webView, body: dataFromString)

        guard let result = handler.proceed() as? String? else {
            os_log("Handler returns wrong result")
            completionHandler(nil)
            return
        }

        completionHandler(result)
    }
}

extension WebViewController: WKScriptMessageHandler{
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let webView = webView else {
            return
        }

        guard let dataDictionary = message.body as? [String : String] else {
            return
        }

        guard let method = JsApi.Method.methodFrom(dictionary: dataDictionary) else {
            os_log("JS data is not valid: \(dataDictionary)")
            return
        }

        let handler = method.createHandler(webView: webView, body: dataDictionary)

        activeHandlers.append(handler)
        handler.proceed { [weak self] _ in
            if let index = self?.activeHandlers.firstIndex(of: handler) {
                self?.activeHandlers.remove(at: index)
            }
        }
    }
}
