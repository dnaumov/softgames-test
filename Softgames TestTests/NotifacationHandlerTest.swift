//
//  NotifacationHandlerTest.swift
//  Softgames TestTests
//
//  Created by Dmytro Naumov on 12.02.22.
//

import XCTest
import WebKit
@testable import Softgames_Test

class NotifacationHandlerTest: XCTestCase {
    private let correctMethodString = "{ \"function\": \"triggerNotification\"}"
    private let incorrectMethodString = "{}"
    private let webView = WKWebView()
    
    func testValidHandler() throws {
        guard let dataFromString = correctMethodString.jsonStringToDictionary(), let method = JsApi.Method.methodFrom(dictionary: dataFromString) else {
            assertionFailure("Method is not created")
            return
        }

        XCTAssertEqual(method, JsApi.Method.notification)

        let handler = method.createHandler(webView: webView, body: dataFromString)
        // As this handler doesn't return any result, just make sure it can be created
        XCTAssertNotNil(handler, "Handler should be created")
    }

    func testInvalidHandlerCreation() throws {
        guard let dataFromString = incorrectMethodString.jsonStringToDictionary() else {
            assertionFailure("Dictionary is not created")
            return
        }

        let method = JsApi.Method.methodFrom(dictionary: dataFromString)
        XCTAssertNil(method, "There are no method for the `NO_FUNCTION`")
    }

}
