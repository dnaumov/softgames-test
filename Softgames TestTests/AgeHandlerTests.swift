//
//  AgeHandlerTests.swift
//  Softgames TestTests
//
//  Created by Dmytro Naumov on 12.02.22.
//

import XCTest
import WebKit
@testable import Softgames_Test

class AgeHandlerTests: XCTestCase {

    private let correctString = "{ \"function\": \"getAge\", \"callbackFunction\": \"getUserAgeCallback\", \"date\": \"20.09.1984\" }"
    private let incorrectMethodString = "{ \"callbackFunction\": \"getUserAgeCallback\", \"date\": \"20.09.1984\" }"
    private let webView = WKWebView()

    func testValidHandler() throws {
        guard let dataFromString = correctString.jsonStringToDictionary(), let method = JsApi.Method.methodFrom(dictionary: dataFromString) else {
            assertionFailure("Method is not created")
            return
        }

        XCTAssertEqual(method, JsApi.Method.age)

        let handler = method.createHandler(webView: webView, body: dataFromString)

        handler.proceed { result in
            let age = result as? Int
            XCTAssertEqual(37, age)
        }
    }

    func testInvalidHandlerCreation() throws {
        guard let dataFromString = incorrectMethodString.jsonStringToDictionary() else {
            assertionFailure("Dictionary is not created")
            return
        }

        let method = JsApi.Method.methodFrom(dictionary: dataFromString)
        XCTAssertNil(method, "There are no method for the `NO_FUNCTION`")
    }

}
