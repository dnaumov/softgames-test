//
//  FullNameHandlerTest.swift
//  Softgames TestTests
//
//  Created by Dmytro Naumov on 12.02.22.
//

import XCTest
@testable import Softgames_Test
import WebKit

class FullNameHandlerTest: XCTestCase {

    private let correctNameString = "{\"function\":\"fullName\", \"firstName\":\"" + "Dmytro"  + "\", \"lastName\":\"" + "Naumov" + "\"}"
    private let incorrectNameString = "{\"function\":\"NO_FUNCTION\", \"firstName\":\"" + "Dmytro"  + "\", \"lastName\":\"" + "Naumov" + "\"}"
    private let webView = WKWebView()

    func testValidHandler() throws {
        guard let dataFromString = correctNameString.jsonStringToDictionary(), let method = JsApi.Method.methodFrom(dictionary: dataFromString) else {
            assertionFailure("Method is not created")
            return
        }

        XCTAssertEqual(method, JsApi.Method.fullName)

        let handler = method.createHandler(webView: webView, body: dataFromString)

        let result = handler.proceed() as? String?
        XCTAssertNotNil(result, "Should be valid result")
        XCTAssertEqual("Dmytro Naumov", result)
    }

    func testInvalidHandlerCreation() throws {
        guard let dataFromString = incorrectNameString.jsonStringToDictionary() else {
            assertionFailure("Dictionary is not created")
            return
        }

        let method = JsApi.Method.methodFrom(dictionary: dataFromString)
        XCTAssertNil(method, "There are no method for the `NO_FUNCTION`")
    }

}
